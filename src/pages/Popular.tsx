import { colors, makeStyles, Theme, Typography } from "@material-ui/core";
import { useQuery } from "react-query";
import { useLoaderData } from "react-router-dom";
import GenreCard from "../components/layout/GenreCard";
import { IGenresData } from "../interfaces/movies/IGenre";
import { IMoviesData } from "../interfaces/movies/IMoviesData";

const PAGE = 1;

const fetchPopularMovies = async (): Promise<IMoviesData> => {
  const res = await fetch(`
  https://api.themoviedb.org/3/movie/popular?api_key=${
    import.meta.env.VITE_API_KEY
  }&language=en-US&page=${PAGE}`);
  return res.json();
};

const Popular = () => {
  const classes = useStyles();

  const genresData = useLoaderData();

  const { data, status, error } = useQuery("popularMovies", fetchPopularMovies);

  if (status === "error" && error instanceof Error) {
    return <div className={classes.error}>{error.message}</div>;
  }

  return (
    <div>
      <Typography variant="h3" className={classes.title}>
        Popular movies
      </Typography>
      <div>
        {typeof genresData === "object" && genresData !== null && data ? (
          (genresData as IGenresData).genres.map((genre) => (
            <GenreCard genre={genre} key={genre.id} moviesData={data} />
          ))
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    color: colors.common.white,
    textAlign: "center",
    marginBottom: theme.spacing(5),
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(2),
    },
  },
  error: {
    color: colors.red[300],
  },
}));

export default Popular;
