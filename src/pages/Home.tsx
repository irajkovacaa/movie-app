import { colors, makeStyles, Theme, Typography } from "@material-ui/core";
import { Link } from "react-router-dom";
import FlexCard from "../components/layout/FlexCard";
import { Routes } from "../constants/routing/routes";

const Home = () => {
  const classes = useStyles();

  return (
    <div>
      <Typography variant="h2" className={classes.title}>
        Movies app
      </Typography>
      <FlexCard
        title="Your movies, all together"
        contentText="Browse your favorite movies and mark them as favorite if you find them interesting."
        containerClassName={classes.flexCardWrapper}
        imgPath="https://s3-us-west-2.amazonaws.com/prd-rteditorial/wp-content/uploads/2018/03/13153742/RT_300EssentialMovies_700X250.jpg"
        linksTo={Routes.Discover}
        linkText="Discover movies"
      />
      <FlexCard
        title="Check out popular movies"
        contentText="Look through the most popular movies people are watching all around the world!"
        containerClassName={classes.flexCardWrapper}
        imgPath="https://eu-images.contentstack.com/v3/assets/bltbc1876152fcd9f07/blt44e5b5250c9b7947/633c05fcfca41f10e35aa574/ironmanthumb.png"
        reverse
      />
      <FlexCard
        title="Check your favorite movie"
        contentText="
        Find out everything about your favorite movie in one place. Click on the movie you like and we will take you to the details page!"
        containerClassName={classes.flexCardWrapper}
        imgPath="https://i0.wp.com/wemadethisnetwork.com/wp-content/uploads/2018/07/maxresdefault-2.jpg?fit=1000%2C563&ssl=1"
      />
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  link: {
    textDecoration: "none",
    marginLeft: theme.spacing(1),
    color: colors.common.white,
  },
  linkTitle: {
    color: colors.common.white,
  },
  title: {
    color: "#F6D239",
    textAlign: "center",
    marginBottom: theme.spacing(7),
  },
  flexCardWrapper: {
    marginBottom: theme.spacing(10),
  },
}));

export default Home;
