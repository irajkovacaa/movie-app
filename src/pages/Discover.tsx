import { colors, makeStyles, Theme, Typography } from "@material-ui/core";
import { QueryFunctionContext, useInfiniteQuery } from "react-query";
import InfiniteScroll from "react-infinite-scroller";
import { IMoviesData } from "../interfaces/movies/IMoviesData";
import MovieCard from "../components/layout/MovieCard";
import { FilterKeys } from "../constants/filter/filterKeys";
import YearFilter from "../components/filter/YearFilter";
import { Tune } from "@material-ui/icons";
import { useEffect, useMemo, useState } from "react";
import { IDiscoverFilters } from "../interfaces/filter/IDiscoverFilters";
import { useLoaderData, useSearchParams } from "react-router-dom";
import { IGenre, IGenresData } from "../interfaces/movies/IGenre";
import { IFilterOption } from "../interfaces/filter/IFilterOption";
import SelectWithAutocomplete from "../components/filter/SelectWithAutocomplete";

const discoverMovies = async ({
  pageParam = 1,
  queryKey,
}: QueryFunctionContext) => {
  const queryParams = queryKey[1] as IDiscoverFilters;

  const yearFilterString = queryParams.year
    ? `&${FilterKeys.Year}=${queryParams.year}`
    : "";
  const genresFilterString = queryParams.genres
    ? `&${FilterKeys.Genres}=${queryParams.genres}`
    : "";

  const response = await fetch(
    `https://api.themoviedb.org/3/discover/movie?api_key=${
      import.meta.env.VITE_API_KEY
    }&page=${pageParam}${yearFilterString && yearFilterString}${
      genresFilterString && genresFilterString
    }`
  );

  if (!response.ok) {
    throw new Error("Something went wrong!");
  }

  const results = response.json();

  return results;
};

const Discover = () => {
  const classes = useStyles();

  const [searchParams, setSearchParams] = useSearchParams();

  const [filters, setFilters] = useState<IDiscoverFilters>({
    year: "",
    genres: "",
  });

  const genresData = useLoaderData();

  const { data, hasNextPage, fetchNextPage } = useInfiniteQuery(
    ["discover-movies", filters],
    discoverMovies,
    {
      getNextPageParam: (lastPage: IMoviesData) => {
        if (lastPage.page < lastPage.total_pages) return lastPage.page + 1;

        return undefined;
      },
    }
  );

  useEffect(() => {
    setFilters({
      genres: searchParams.get(FilterKeys.Genres) || "",
      year: searchParams.get(FilterKeys.Year) || "",
    });
  }, [searchParams]);

  const onYearChange = (year: number | null) => {
    setFilters((prev) => ({
      ...prev,
      year: year ? year.toString() : "",
    }));

    searchParams.delete(FilterKeys.Year);
    year && searchParams.append(FilterKeys.Year, year.toString());
    setSearchParams(searchParams);
  };

  const genreOptions: IFilterOption[] = useMemo(() => {
    if (typeof genresData === "object" && genresData !== null) {
      const genresDataObj = genresData as IGenresData;

      return genresDataObj.genres.map((genre: IGenre) => ({
        value: genre.id.toString(),
        label: genre.name,
      }));
    }

    return [];
  }, [genresData]);

  const onGenreChange = (values: IFilterOption[]) => {
    setFilters((prev) => ({
      ...prev,
      genres: values.map((val) => val.value).join(),
    }));

    searchParams.delete(FilterKeys.Genres);
    values.length > 0 &&
      searchParams.append(
        FilterKeys.Genres,
        values.map((val) => val.value).join()
      );

    setSearchParams(searchParams);
  };

  return (
    <div>
      <Typography variant="h3" className={classes.title}>
        Discover movies
      </Typography>
      <div className={classes.filterSection}>
        <Tune className={classes.filterIcon} />
        <YearFilter
          title="Year"
          paramsKey={FilterKeys.Year}
          onYearChange={onYearChange}
          wrapperClassName={classes.datePicker}
        />
        <SelectWithAutocomplete
          title="Genre"
          paramsKey={FilterKeys.Genres}
          options={genreOptions}
          wrapperClassName={classes.genreWrapper}
          handleChange={onGenreChange}
        />
      </div>
      <InfiniteScroll
        className={classes.infiniteScrollWrapper}
        hasMore={hasNextPage}
        loadMore={(page) => {
          fetchNextPage({ pageParam: page });
        }}
      >
        {data && data.pages[0].results.length > 0 ? (
          data.pages.map((pageData: IMoviesData) =>
            pageData.results.map((movie) => (
              <MovieCard isSearch={false} movie={movie} key={movie.id} />
            ))
          )
        ) : (
          <Typography className={classes.noResultsText}>
            No movies to show!
          </Typography>
        )}
      </InfiniteScroll>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    color: colors.common.white,
    textAlign: "center",
    marginBottom: theme.spacing(5),
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(2),
    },
  },
  filterSection: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "100%",
    padding: "0 20px",
    marginBottom: theme.spacing(3),
    [theme.breakpoints.down("sm")]: {
      flexDirection: "column",
    },
  },
  filterIcon: {
    fill: "#F6D239",
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(3),
    },
  },
  datePicker: {
    marginLeft: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 0,
      marginBottom: theme.spacing(2),
    },
  },
  infiniteScrollWrapper: {
    display: "flex",
    flexWrap: "wrap",
    justifyContent: "center",
  },
  genreWrapper: {
    marginLeft: theme.spacing(3),
    [theme.breakpoints.down("sm")]: {
      marginLeft: 0,
    },
  },
  noResultsText: {
    color: colors.common.white,
    textAlign: "center",
    marginTop: theme.spacing(2),
  },
}));

export default Discover;
