import {
  Button,
  colors,
  makeStyles,
  Theme,
  Typography,
} from "@material-ui/core";
import { useLoaderData, useParams } from "react-router-dom";
import { IMovieDetail } from "../interfaces/movies/IMovieDetail";
import { useFavorite } from "../utils/useFavorite";

const MovieDetails = () => {
  const movieDetails = useLoaderData();

  const classes = useStyles();

  const { id } = useParams();

  const [isFavorite, toggleFavorite] = useFavorite(id);

  if (typeof movieDetails !== "object" || movieDetails === null) {
    return <></>;
  }

  const movieData = movieDetails as IMovieDetail;

  return (
    <div className={classes.container}>
      <Typography variant="h3" className={classes.title}>
        {movieData.title}
      </Typography>
      {movieData.poster_path && (
        <img
          src={`http://image.tmdb.org/t/p/w500/${movieData.poster_path}`}
          alt={`${movieData.title}-poster`}
          className={classes.posterImg}
        />
      )}
      <Typography variant="body1" className={classes.overview}>
        {movieData.overview}
      </Typography>
      <Typography variant="body1" className={classes.overview}>
        {`Genres : ${movieData.genres.map((genre) => genre.name).join()}`}
      </Typography>
      <Typography variant="body1" className={classes.overview}>
        {`Released : ${movieData.release_date}`}
      </Typography>
      <Button
        onClick={() => {
          toggleFavorite(movieData.title);
        }}
        className={`${classes.favButton} ${
          isFavorite && classes.favButtonRemove
        }`}
      >
        {isFavorite ? "Remove from favorites" : "Add to favorites"}
      </Button>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    color: colors.common.white,
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    textAlign: "center",
  },
  title: {
    textAlign: "center",
    marginBottom: theme.spacing(5),
    [theme.breakpoints.down("sm")]: {
      marginBottom: theme.spacing(2),
    },
  },
  posterImg: {
    display: "block",
    width: "100%",
    maxWidth: 200,
    marginBottom: theme.spacing(3),
  },
  overview: {
    maxWidth: 400,
    marginBottom: theme.spacing(3),
  },
  favButton: {
    borderRadius: 24,
    color: colors.common.white,
    backgroundColor: "#F6D239",
  },
  favButtonRemove: {
    backgroundColor: "#cc0000",
  },
}));

export default MovieDetails;
