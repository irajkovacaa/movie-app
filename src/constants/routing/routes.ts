export enum Routes {
  Home = "/",
  Discover = "/discover",
  MovieDetailsBase = "/movies/",
  MovieDetails = "/movies/:id",
  Popular = "/popular",
}
