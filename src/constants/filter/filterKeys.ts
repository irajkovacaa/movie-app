export enum FilterKeys {
  Year = "year",
  Genres = "with_genres",
  Query = "query",
}
