import { colors, createTheme } from "@material-ui/core";

export const DefaultTheme = createTheme({
  palette: {
    primary: {
      main: colors.common.white,
    },
  },
  overrides: {
    MuiInputBase: {
      input: {
        "&:-webkit-autofill": {
          "-webkit-background-clip": "text",
        },
      },
    },
    MuiFormLabel: {
      root: {
        color: colors.common.white,
      },
    },
    MuiFormControl: {
      root: {
        minWidth: 120,
        maxWidth: 300,
      },
    },
    MuiInput: {
      root: {
        color: colors.common.white,
      },
      underline: {
        "&:before": {
          border: "white !important",
        },
        "&:after": {
          border: "white !important",
        },
      },
    },
    MuiOutlinedInput: {
      notchedOutline: {
        borderColor: colors.common.white,
      },
    },
  },
});
