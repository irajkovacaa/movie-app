import {
  Button,
  colors,
  makeStyles,
  MenuItem,
  Popover,
  Theme,
  Typography,
} from "@material-ui/core";
import { ArrowDropDown } from "@material-ui/icons";
import React from "react";
import { Link } from "react-router-dom";
import { Routes } from "../../constants/routing/routes";
import { LocalStorageKeys } from "../../constants/storage/localStorageKeys";
import { ILocalStorageMovie } from "../../interfaces/storage/ILocalStorageMovie";

const FavoritesPopover = () => {
  const classes = useStyles();

  const [anchorEl, setAnchorEl] = React.useState<HTMLButtonElement | null>(
    null
  );

  const favorites = localStorage.getItem(LocalStorageKeys.Favorites);

  const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);
  const id = open ? "favorites-popover" : undefined;

  return (
    <div>
      <Button
        aria-describedby={id}
        variant="contained"
        className={classes.favoritesBtn}
        onClick={handleClick}
        endIcon={<ArrowDropDown />}
      >
        Favorites
      </Button>
      <Popover
        id={id}
        open={open}
        anchorEl={anchorEl}
        onClose={handleClose}
        anchorOrigin={{
          vertical: "bottom",
          horizontal: "center",
        }}
        transformOrigin={{
          vertical: "top",
          horizontal: "center",
        }}
      >
        {favorites ? (
          <div className={classes.favoritesList}>
            {(JSON.parse(favorites) as ILocalStorageMovie[]).map((favorite) => (
              <MenuItem
                classes={{
                  root: classes.menuItem,
                }}
                key={favorite.id}
                component={Link}
                to={`${Routes.MovieDetailsBase}${favorite.id}`}
              >
                {favorite.title}
              </MenuItem>
            ))}
          </div>
        ) : (
          <Typography className={classes.typography}>
            You dont have any favorite movies!
          </Typography>
        )}
      </Popover>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  typography: {
    padding: theme.spacing(2),
  },
  favoritesList: {
    padding: theme.spacing(2),
    maxHeight: 300,
    overflowY: "auto",
  },
  menuItem: {
    padding: "8px 16px",
    borderRadius: "24px",
    fontSize: "16px",
    color: colors.common.black,
  },
  favoritesBtn: {
    backgroundColor: "#F6D239",
  },
}));
export default FavoritesPopover;
