import {
  AppBar,
  colors,
  CssBaseline,
  IconButton,
  makeStyles,
  Toolbar,
  useMediaQuery,
} from "@material-ui/core";
import { Menu } from "@material-ui/icons";
import { useEffect, useState } from "react";
import { Outlet, useLocation } from "react-router-dom";
import MoviesSearch from "../filter/MoviesSearch";
import FavoritesPopover from "./FavoritesPopover";
import Sidebar from "./Sidebar";

export const drawerWidth = 200;

const MainLayout = () => {
  const isMobile = useMediaQuery("(max-width: 599px)");
  const classes = useStyles({ isMobile });
  const [open, setOpen] = useState(false);

  const location = useLocation();

  useEffect(() => {
    //close sidebar on route change
    setOpen(false);
  }, [location]);

  const handleDrawerToggle = () => {
    setOpen(!open);
  };

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="fixed" elevation={0} className={classes.appBar}>
        <Toolbar classes={{ gutters: classes.gutters }}>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            edge="start"
            onClick={handleDrawerToggle}
            className={classes.menuButton}
          >
            <Menu />
          </IconButton>
          <div className={classes.sideWrapper}>
            <FavoritesPopover />
            <MoviesSearch wrapperClassName={classes.searchInputWrapper} />
          </div>
        </Toolbar>
      </AppBar>
      <Sidebar open={open} handleDrawerToggle={handleDrawerToggle} />
      <main className={classes.content}>
        <Outlet />
      </main>
    </div>
  );
};

export interface ClassProps {
  isMobile: boolean;
}

const useStyles = makeStyles((theme) => ({
  "@global": {
    "*::-webkit-scrollbar": {
      width: "8px",
      height: "6px",
    },
    "*::-webkit-scrollbar-track": {
      background: `${colors.grey[200]}`,
      borderRadius: "12px",
    },
    "*::-webkit-scrollbar-thumb": {
      background: `rgba(6,13,23,.8)`,
      borderRadius: "12px",
    },
  },
  gutters: {
    paddingLeft: 40,
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("xs")]: {
      flexDirection: "column",
      paddingBottom: theme.spacing(1),
    },
  },
  root: {
    display: "flex",
    boxSizing: "border-box",
    minHeight: "100vh",
    backgroundColor: "rgba(6,13,23,.8)",
    background: "linear-gradient(180deg,#060d17,rgba(6,13,23,.85))",
  },
  content: {
    transition: "all .5s",
    padding: theme.spacing(10, 3, 3, 3),
    width: "100%",
    maxWidth: 1600,
    margin: "0 auto",
    [theme.breakpoints.down("xs")]: {
      paddingTop: theme.spacing(14),
    },
  },
  appBar: {
    transition: "width .5s",
    backgroundColor: "rgba(6,13,23,.7)",
    backdropFilter: "blur(20px)",
    width: "100%",
  },

  menuButton: {
    display: "block",
    color: colors.common.white,
    marginRight: theme.spacing(2),
  },
  searchInputWrapper: {
    marginLeft: theme.spacing(5),
    [theme.breakpoints.down("xs")]: {
      marginLeft: theme.spacing(2),
    },
  },
  sideWrapper: {
    display: "flex",
  },
}));

export default MainLayout;
