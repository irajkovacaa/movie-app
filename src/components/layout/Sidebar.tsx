import {
  colors,
  Drawer,
  makeStyles,
  MenuItem,
  MenuList,
} from "@material-ui/core";
import { useMemo } from "react";
import { Link } from "react-router-dom";
import { Routes } from "../../constants/routing/routes";
import { drawerWidth } from "./MainLayout";

interface Props {
  open: boolean;
  handleDrawerToggle: () => void;
}

const Sidebar = (props: Props) => {
  const { open, handleDrawerToggle } = props;

  const classes = useStyles();

  const menu = useMemo(
    () => (
      <MenuList disablePadding className={classes.menuList}>
        <MenuItem
          className={classes.logoMenuItem}
          classes={{
            root: classes.menuItem,
          }}
          component={Link}
          to={Routes.Home}
        >
          Movies
        </MenuItem>
        <MenuItem
          classes={{
            root: classes.menuItem,
          }}
          component={Link}
          to={Routes.Home}
        >
          Home
        </MenuItem>
        <MenuItem
          classes={{
            root: classes.menuItem,
          }}
          component={Link}
          to={Routes.Discover}
        >
          Discover
        </MenuItem>
        <MenuItem
          classes={{
            root: classes.menuItem,
          }}
          component={Link}
          to={Routes.Popular}
        >
          Popular
        </MenuItem>
      </MenuList>
    ),
    [classes.logoMenuItem, classes.menuItem, classes.menuList]
  );

  return (
    <nav>
      <Drawer
        variant="temporary"
        anchor={"left"}
        open={open}
        onClose={handleDrawerToggle}
        classes={{
          paper: classes.drawerPaper,
        }}
        ModalProps={{
          keepMounted: true, // Better open performance on mobile.
        }}
      >
        {menu}
      </Drawer>
    </nav>
  );
};

const useStyles = makeStyles((theme) => ({
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: `${drawerWidth}px`,
    display: "flex",
    flexDirection: "column",
    transition: "all .5s",
    overflowX: "hidden",
    background: colors.common.black,
  },
  dividerMenu: {
    margin: "12px auto",
    width: "176px",
  },
  divider: {
    marginBottom: "20px",
    width: "176px",
    marginLeft: "auto",
    marginRight: "auto",
  },
  drawerAnchorLeft: {
    border: 0,
  },
  menu: {
    flexGrow: 1,
  },
  menuItem: {
    minHeight: "auto",
    display: "inline-flex",
    alignItems: "center",
    padding: "8px 16px",
    borderRadius: "24px",
    fontSize: "18px",
    color: colors.common.white,
  },
  menuList: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing(4, 0, 0, 3),
  },
  logoMenuItem: {
    marginBottom: theme.spacing(4),
    color: "#F6D239",
  },
}));

export default Sidebar;
