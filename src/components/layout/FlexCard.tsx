import { colors, makeStyles, Theme, Typography } from "@material-ui/core";
import React from "react";
import { Link } from "react-router-dom";

interface Props {
  containerClassName?: string;
  reverse?: boolean;
  title: string;
  contentText: string;
  imgPath: string;
  linksTo?: string;
  linkText?: string;
}

const FlexCard = ({
  containerClassName,
  reverse,
  title,
  contentText,
  imgPath,
  linksTo,
  linkText,
}: Props) => {
  const classes = useStyles();

  return (
    <div
      className={`${classes.container} ${containerClassName} ${
        reverse && classes.containerReverse
      } `}
    >
      <div className={classes.content}>
        <Typography variant="h4" className={classes.title}>
          {title}
        </Typography>
        <Typography variant="body1" className={classes.contentText}>
          {contentText}
        </Typography>
        {linksTo && (
          <Link to={linksTo} className={classes.link}>
            <Typography className={classes.linkTitle}>
              {linkText || ""}
            </Typography>
          </Link>
        )}
      </div>
      <div className={classes.imgWrapper}>
        <img
          src={imgPath}
          alt="cinema-people"
          loading="lazy"
          className={classes.img}
        />
      </div>
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    display: "flex",
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-evenly",
    flexWrap: "wrap",
    color: colors.common.white,
    maxWidth: 1500,
    textAlign: "center",
  },
  containerReverse: {
    flexDirection: "row-reverse",
  },
  content: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: theme.spacing(0, 4),
    maxWidth: 500,
  },
  title: {
    color: colors.common.white,
    marginBottom: theme.spacing(2),
    fontWeight: "bold",
  },
  contentText: {
    marginBottom: theme.spacing(3),
    color: colors.grey[500],
  },
  imgWrapper: {
    display: "flex",
    justifyContent: "center",
    padding: theme.spacing(3, 0),
  },
  img: {
    display: "block",
    width: "500px",
    [theme.breakpoints.down("sm")]: {
      width: "400px",
    },
    [theme.breakpoints.down("xs")]: {
      width: "250px",
    },
  },
  link: {
    textDecoration: "none",
    marginLeft: theme.spacing(3),
    color: colors.common.white,
    borderRadius: "24px",
    backgroundColor: "#F6D239",
    padding: theme.spacing(1, 3),
  },
  linkTitle: {
    color: colors.common.white,
  },
}));

export default FlexCard;
