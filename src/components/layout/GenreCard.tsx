import { makeStyles, Theme, Typography } from "@material-ui/core";
import React, { useMemo } from "react";
import { IGenre } from "../../interfaces/movies/IGenre";
import { IMovie } from "../../interfaces/movies/IMovie";
import { IMoviesData } from "../../interfaces/movies/IMoviesData";
import MovieCard from "./MovieCard";

interface Props {
  genre: IGenre;
  moviesData: IMoviesData;
}

const GenreCard = ({ genre, moviesData }: Props) => {
  const classes = useStyles();

  const popularMoviesForGenre: IMovie[] = useMemo(() => {
    const movies = moviesData.results.filter((movie) =>
      movie.genre_ids.includes(genre.id)
    );
    return movies;
  }, [genre.id, moviesData.results]);

  return popularMoviesForGenre.length > 0 ? (
    <div>
      <Typography className={classes.title} variant="h5">
        {genre.name}
      </Typography>
      <div className={classes.moviesContainer}>
        {popularMoviesForGenre.map((movie) => (
          <MovieCard
            movie={movie}
            isSearch={false}
            key={movie.id}
            wrapperClassName={classes.movieCard}
          />
        ))}
      </div>
    </div>
  ) : (
    <></>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  title: {
    textAlign: "center",
    marginBottom: theme.spacing(3),
    color: "#F6D239",
  },
  moviesContainer: {
    width: "100%",
    display: "flex",
    overflowX: "auto",
    marginBottom: theme.spacing(5),
  },
  movieCard: {
    margin: theme.spacing(0, 3),
  },
}));

export default GenreCard;
