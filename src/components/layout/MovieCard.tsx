import {
  Theme,
  makeStyles,
  Typography,
  colors,
  IconButton,
} from "@material-ui/core";
import { Star } from "@material-ui/icons";
import React from "react";
import { useNavigate } from "react-router-dom";
import { Routes } from "../../constants/routing/routes";
import { IMovie } from "../../interfaces/movies/IMovie";
import { useFavorite } from "../../utils/useFavorite";

interface Props {
  movie: IMovie;
  isSearch: boolean;
  wrapperClassName?: string;
}

const MovieCard = ({ movie, isSearch, wrapperClassName }: Props) => {
  const classes = useStyles();

  const [isFavorite, toggleFavorite] = useFavorite(movie.id.toString());

  const navigate = useNavigate();

  const navigateToDetails = () => {
    navigate(`${Routes.MovieDetailsBase}${movie.id.toString()}`);
  };

  return (
    <div
      className={`${
        isSearch ? classes.cardSearch : classes.card
      } ${wrapperClassName}`}
      onClick={navigateToDetails}
    >
      <img
        className={isSearch ? classes.imageSearch : classes.image}
        loading="lazy"
        src={
          movie.poster_path
            ? `http://image.tmdb.org/t/p/w500/${movie.poster_path}`
            : undefined
        }
        alt={`${movie.title}-image`}
      />
      <Typography className={isSearch ? classes.titleSearch : classes.title}>
        {movie.title}
      </Typography>

      {!isSearch ? (
        <IconButton
          onClick={(e) => {
            e.stopPropagation();
            toggleFavorite(movie.title);
          }}
          className={classes.starWrapper}
        >
          <Star
            className={`${classes.star} ${isFavorite && classes.starFavorite}`}
          />
        </IconButton>
      ) : null}
    </div>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  card: {
    width: 260,
    borderRadius: 4,
    padding: theme.spacing(2, 2.5),
    margin: theme.spacing(2),
    display: "inline-flex",
    flexDirection: "column",
    alignItems: "center",
    position: "relative",
    cursor: "pointer",
    textAlign: "center",
    [theme.breakpoints.down("xs")]: {
      maxWidth: 220,
    },
  },
  cardSearch: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    marginBottom: theme.spacing(2),
    padding: theme.spacing(1),
    cursor: "pointer",
    "&:hover": {
      opacity: "0.6",
    },
  },
  title: {
    color: colors.common.white,
    marginBottom: theme.spacing(2),
  },

  titleSearch: {
    color: colors.common.white,
    marginLeft: theme.spacing(2),
  },

  image: {
    display: "block",
    width: "260px",
    marginBottom: theme.spacing(2),
  },

  imageSearch: {
    display: "block",
    width: "45px",
    [theme.breakpoints.down("sm")]: {
      width: "30px",
    },
  },
  starWrapper: {
    position: "absolute",
    top: 0,
    left: 0,
  },
  star: {
    transform: "scale(1.6)",
    fill: colors.common.white,
  },
  starFavorite: {
    fill: "#F6D239",
  },
}));

export default MovieCard;
