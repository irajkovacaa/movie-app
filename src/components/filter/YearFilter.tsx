import React, { useEffect, useState } from "react";
import { MuiPickersUtilsProvider, DatePicker } from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import { MaterialUiPickersDate } from "@material-ui/pickers/typings/date";
import moment, { Moment } from "moment";
import { createTheme, ThemeProvider, colors } from "@material-ui/core";
import { Clear } from "@material-ui/icons";
import { useSearchParams } from "react-router-dom";

interface Props {
  title: string;
  wrapperClassName?: string;
  onYearChange: (year: number | null) => void;
  paramsKey?: string;
}

const YearFilter = ({
  title,
  wrapperClassName,
  paramsKey,
  onYearChange,
}: Props) => {
  const [selectedDate, setSelectedDate] = useState<Moment | null>(null);

  const [open, setOpen] = useState(false);

  const [searchParams] = useSearchParams();

  useEffect(() => {
    if (paramsKey) {
      const defaultValue = searchParams.get(paramsKey);
      defaultValue && setSelectedDate(moment().year(parseInt(defaultValue)));
    }
  }, [paramsKey, searchParams]);

  const onDateChange = (date: MaterialUiPickersDate) => {
    setSelectedDate(moment(date));
  };

  const handleClear = () => {
    setSelectedDate(null);
  };

  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <ThemeProvider theme={datePickerTheme}>
        <DatePicker
          className={wrapperClassName}
          disableFuture
          views={["year"]}
          label={title}
          value={selectedDate}
          inputVariant="outlined"
          open={open}
          onFocus={() => {
            setOpen(true);
          }}
          onClose={() => {
            setOpen(false);
          }}
          onChange={(e) => {
            onDateChange(e);
            onYearChange(moment(e).year());
          }}
          maxDate={moment()}
          InputProps={{
            endAdornment: (
              <div
                onClick={() => {
                  handleClear();
                  onYearChange(null);
                }}
                style={{
                  marginRight: 20,
                  cursor: "pointer",
                  color: "white",
                  transform: "scale(0.85)",
                }}
              >
                <Clear />
              </div>
            ),
          }}
        />
      </ThemeProvider>
    </MuiPickersUtilsProvider>
  );
};

const datePickerTheme = createTheme({
  overrides: {
    MuiFormLabel: {
      root: {
        color: `${colors.common.white} !important`,
      },
    },
    MuiInputLabel: {
      outlined: {
        transform: "translate(14px,12px) scale(1)",
      },
    },
    MuiOutlinedInput: {
      input: {
        cursor: "pointer",
        color: colors.common.white,
        padding: "10.5px 0px",
        paddingLeft: "12px",
      },
      notchedOutline: {
        borderColor: colors.common.white,
      },
    },
    MuiInput: {
      root: {
        color: colors.common.white,
      },
      underline: {
        "&:before": {
          border: "none !important",
        },
        "&:after": {
          border: "none !important",
        },
      },
    },

    MuiToolbar: {
      root: {
        background: "linear-gradient(180deg,#060d17,rgba(6,13,23,.85))",
      },
    },
    MuiButton: {
      label: {
        color: colors.common.black,
      },
    },
  },
});

export default YearFilter;
