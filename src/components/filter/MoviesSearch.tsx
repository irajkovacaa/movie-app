import React, { ChangeEvent, useEffect, useState } from "react";
import TextField from "@material-ui/core/TextField";
import { FilterKeys } from "../../constants/filter/filterKeys";
import { IMoviesData } from "../../interfaces/movies/IMoviesData";
import {
  Theme,
  makeStyles,
  colors,
  InputAdornment,
  ClickAwayListener,
  Typography,
} from "@material-ui/core";
import { Search } from "@material-ui/icons";
import MovieCard from "../layout/MovieCard";
import { QueryFunctionContext, useInfiniteQuery } from "react-query";
import { ISearchFilter } from "../../interfaces/filter/ISearchFilter";
import InfiniteScroll from "react-infinite-scroller";
import { useLocation } from "react-router-dom";

const searchMovies = async ({ queryKey }: QueryFunctionContext) => {
  const queryParams = queryKey[1] as ISearchFilter;

  if (!queryParams.searchTerm) {
    throw new Error("No query!");
  }
  const response = await fetch(
    `https://api.themoviedb.org/3/search/movie?api_key=${
      import.meta.env.VITE_API_KEY
    }&language=en-US&${FilterKeys.Query}=${queryParams.searchTerm}`
  );

  if (!response.ok) {
    throw new Error("Something went wrong!");
  }

  const results = response.json();

  return results;
};

interface Props {
  wrapperClassName?: string;
}

const MoviesSearch = ({ wrapperClassName }: Props) => {
  const [inputValue, setInputValue] = React.useState("");
  const [open, setOpen] = useState(false);

  const classes = useStyles();

  const location = useLocation();

  const [filters, setFilters] = useState<ISearchFilter>({
    searchTerm: "",
  });

  const { data, hasNextPage, fetchNextPage } = useInfiniteQuery(
    ["search-movies", filters],
    searchMovies,
    {
      getNextPageParam: (lastPage: IMoviesData) => {
        if (lastPage.page < lastPage.total_pages) return lastPage.page + 1;

        return undefined;
      },
    }
  );

  useEffect(() => {
    //reset input on route change
    setFilters({
      searchTerm: "",
    });
    setInputValue("");
    setOpen(false);
  }, [location]);

  useEffect(() => {
    const searchTimeout = setTimeout(() => {
      const searchFilter: ISearchFilter = { searchTerm: inputValue };
      setFilters(searchFilter);
    }, 1000);

    return () => clearTimeout(searchTimeout);
  }, [inputValue]);

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.target.value);
  };

  const openMenu = () => {
    setOpen(true);
  };

  const closeMenu = () => {
    setOpen(false);
  };

  return (
    <ClickAwayListener onClickAway={closeMenu}>
      <div className={`${classes.container} ${wrapperClassName}`}>
        <TextField
          margin="none"
          onChange={handleChange}
          className={classes.input}
          placeholder={"Search movies"}
          value={inputValue}
          classes={{ root: classes.textField }}
          onFocus={openMenu}
          InputProps={{
            className: classes.inputText,
            disableUnderline: true,
            startAdornment: (
              <InputAdornment position="start">
                <Search className={classes.searchIcon} />
              </InputAdornment>
            ),
          }}
        />

        {open ? (
          <div className={classes.moviesList}>
            <InfiniteScroll
              hasMore={hasNextPage}
              useWindow={false}
              loadMore={(page) => {
                fetchNextPage({ pageParam: page });
              }}
            >
              {data && data.pages[0].results.length > 0 ? (
                data.pages.map((pageData: IMoviesData) =>
                  pageData.results.map((movie) => (
                    <MovieCard isSearch={true} movie={movie} key={movie.id} />
                  ))
                )
              ) : (
                <Typography className={classes.noOptionsText}>
                  No movies to show!
                </Typography>
              )}
            </InfiniteScroll>
          </div>
        ) : null}
      </div>
    </ClickAwayListener>
  );
};

const useStyles = makeStyles((theme: Theme) => ({
  container: {
    position: "relative",
  },
  moviesList: {
    position: "absolute",
    width: 500,
    height: 300,
    overflowY: "auto",
    top: 40,
    right: 0,
    borderRadius: 24,
    backgroundColor: "aqua",
    boxShadow: "0px 4px 4px rgba(26, 26, 26, 0.05)",
    background: "linear-gradient(180deg,#060d17,rgba(6,13,23,.85))",
    [theme.breakpoints.down("sm")]: {
      width: "calc(100% - 24px)",
      left: "50%",
      transform: "translateX(-50%)",
      maxWidth: 400,
      height: 200,
    },
  },
  input: {
    borderColor: colors.grey[200],
    backgroundColor: colors.common.white,
    border: "1px solid",
    borderRadius: "16px",
    paddingLeft: "18px",
    boxSizing: "border-box",
    flexGrow: 1,
    minWidth: "290px",
    height: "32px",
    [theme.breakpoints.down("sm")]: {
      minWidth: "unset",
      width: "100%",
    },
  },
  textField: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
  },
  searchIcon: {
    [theme.breakpoints.down("xs")]: {
      transform: "scale(0.7)",
    },
  },
  inputText: {
    color: colors.common.black,
  },
  noOptionsText: {
    color: colors.common.white,
    textAlign: "center",
    marginTop: theme.spacing(2),
  },
}));

export default MoviesSearch;
