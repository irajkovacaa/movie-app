import { colors, makeStyles, TextField, Theme } from "@material-ui/core";
import { Autocomplete } from "@material-ui/lab";
import { useEffect, useState } from "react";
import { useSearchParams } from "react-router-dom";
import { IFilterOption } from "../../interfaces/filter/IFilterOption";

interface Props {
  title: string;
  options: IFilterOption[];
  handleChange: (values: IFilterOption[]) => void;
  wrapperClassName?: string;
  paramsKey?: string;
}

export default function SelectWithAutocomplete({
  options,
  title,
  wrapperClassName,
  handleChange,
  paramsKey,
}: Props) {
  const classes = useStyles();

  const [value, setValue] = useState<IFilterOption[]>([]);

  const [searchParams] = useSearchParams();

  useEffect(() => {
    if (paramsKey) {
      const defaultValues = searchParams.get(paramsKey);
      if (defaultValues) {
        const selectedValues = defaultValues.split(",");
        const selectedOptions = options.filter((val) =>
          selectedValues.some((selected) => selected === val.value)
        );
        setValue(selectedOptions);
      }
    }
  }, [options, paramsKey, searchParams]);

  return (
    <div className={`${classes.autocomplete} ${wrapperClassName}`}>
      <Autocomplete
        multiple
        limitTags={2}
        value={value}
        options={options}
        getOptionLabel={(option: IFilterOption) => option.label}
        onChange={(_, newValue) => {
          handleChange(newValue);
          setValue([...newValue]);
        }}
        size="small"
        renderInput={(params) => (
          <TextField
            {...params}
            variant="outlined"
            label={title}
            placeholder={title}
          />
        )}
      />
    </div>
  );
}

const useStyles = makeStyles((theme: Theme) => ({
  autocomplete: {
    maxWidth: 500,
    "& > * + *": {
      marginTop: theme.spacing(3),
    },
    "& .MuiAutocomplete-clearIndicator": {
      color: colors.common.white,
    },
    "& .MuiAutocomplete-popupIndicator": {
      color: colors.common.white,
    },
    "& .MuiAutocomplete-tag": {
      color: colors.common.white,
    },
    "& .MuiChip-label": {
      color: colors.common.black,
    },
    "& .MuiInputBase-input": {
      color: colors.common.white,
    },
    "& .MuiAutocomplete-inputRoot": {
      flexWrap: "nowrap",
    },
    "& .Mui-focused": {
      flexWrap: "wrap",
    },
  },
}));
