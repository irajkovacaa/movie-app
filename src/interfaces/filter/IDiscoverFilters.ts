export interface IDiscoverFilters {
  year: string;
  genres: string;
}
