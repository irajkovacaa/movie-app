export interface IFilterOption {
  value: string;
  label: string;
}
