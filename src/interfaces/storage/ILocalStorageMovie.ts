export interface ILocalStorageMovie {
  id: string;
  title: string;
}
