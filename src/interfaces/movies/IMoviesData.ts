import { IMovie } from "./IMovie";

export interface IMoviesData {
  page: number;
  results: IMovie[];
  total_results: number;
  total_pages: number;
}
