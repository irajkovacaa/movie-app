import { IGenre } from "./IGenre";
import { IMovie } from "./IMovie";
import { IProductionCompany } from "./IProductionCompany";
import { IProductionCountry } from "./IProductionCountry";
import { ISpokenLanguage } from "./ISpokenLanguage";

export interface IMovieDetail extends Omit<IMovie, "genre_ids"> {
  genres: IGenre[];
  belongs_to_collection: null;
  budget: number;
  homepage: string | null;
  imdb_id: string | null;
  production_companies: IProductionCompany[];
  production_countries: IProductionCountry[];
  revenue: number;
  runtime: number | null;
  spoken_languages: ISpokenLanguage[];
  status: string;
  tagline: string | null;
}
