import React from "react";
import ReactDOM from "react-dom/client";
import {
  createBrowserRouter,
  redirect,
  RouterProvider,
} from "react-router-dom";
import MainLayout from "./components/layout/MainLayout";
import Home from "./pages/Home";
import MovieDetails from "./pages/MovieDetails";
import "./index.css";
import Discover from "./pages/Discover";
import { QueryClient, QueryClientProvider } from "react-query";
import { Routes } from "./constants/routing/routes";
import { ThemeProvider } from "@material-ui/core";
import { DefaultTheme } from "./constants/style/themes";
import Popular from "./pages/Popular";

const queryClient = new QueryClient();

const router = createBrowserRouter([
  {
    path: "/",
    element: <MainLayout />,
    children: [
      {
        //index route matches parent path
        element: <Home />,
        index: true,
      },
      {
        loader: async () => {
          const res = await fetch(`
          https://api.themoviedb.org/3/genre/movie/list?api_key=${
            import.meta.env.VITE_API_KEY
          }&language=en-US`);

          const data = await res.json();

          return data;
        },
        path: Routes.Discover,
        element: <Discover />,
      },
      {
        loader: async ({ params }) => {
          const res = await fetch(
            `https://api.themoviedb.org/3/movie/${params.id}?api_key=${
              import.meta.env.VITE_API_KEY
            }&language=en-US`
          );

          if (!res.ok) {
            throw redirect(Routes.Home);
          }

          const data = await res.json();

          return data;
        },
        path: Routes.MovieDetails,
        element: <MovieDetails />,
      },
      {
        loader: async () => {
          const res = await fetch(`
          https://api.themoviedb.org/3/genre/movie/list?api_key=${
            import.meta.env.VITE_API_KEY
          }&language=en-US`);

          const data = await res.json();

          return data;
        },
        path: Routes.Popular,
        element: <Popular />,
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <QueryClientProvider client={queryClient}>
      <ThemeProvider theme={DefaultTheme}>
        <RouterProvider router={router} />
      </ThemeProvider>
    </QueryClientProvider>
  </React.StrictMode>
);
