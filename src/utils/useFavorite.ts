import { useCallback, useEffect, useState } from "react";
import { LocalStorageKeys } from "../constants/storage/localStorageKeys";
import { ILocalStorageMovie } from "../interfaces/storage/ILocalStorageMovie";

export const useFavorite = (
  movieId: string | undefined
): [boolean, (movieTitle: string) => void] => {
  const [isFavorite, setIsFavorite] = useState(false);

  useEffect(() => {
    const favorites = localStorage.getItem(LocalStorageKeys.Favorites);
    if (favorites && movieId) {
      const favoritesList: ILocalStorageMovie[] = JSON.parse(favorites);
      if (favoritesList.some((movie) => movie.id === movieId)) {
        setIsFavorite(true);
      }
    }
  }, [movieId]);

  const toggleFavorite = useCallback(
    (movieTitle: string) => {
      if (!movieId) return;
      const favorites = localStorage.getItem(LocalStorageKeys.Favorites);
      if (favorites) {
        const favoritesList: ILocalStorageMovie[] = JSON.parse(favorites);
        if (favoritesList.some((movie) => movie.id === movieId)) {
          setIsFavorite(false);
          const newFavorites = favoritesList.filter(
            (movie) => movie.id !== movieId
          );
          localStorage.setItem(
            LocalStorageKeys.Favorites,
            JSON.stringify(newFavorites)
          );
        } else {
          const movieToAdd: ILocalStorageMovie = {
            id: movieId,
            title: movieTitle,
          };
          const newFavorites = [...favoritesList, movieToAdd];
          localStorage.setItem(
            LocalStorageKeys.Favorites,
            JSON.stringify(newFavorites)
          );
          setIsFavorite(true);
        }
      } else {
        const movieToAdd: ILocalStorageMovie = {
          id: movieId,
          title: movieTitle,
        };
        const newFavorites = [movieToAdd];
        localStorage.setItem(
          LocalStorageKeys.Favorites,
          JSON.stringify(newFavorites)
        );
        setIsFavorite(true);
      }
    },
    [movieId]
  );

  return [isFavorite, toggleFavorite];
};
